import "./App.css";
import React from "react";

class Text extends React.Component {
  render() {
    return <span style={{ color: this.props.color }}> {this.props.value}</span>;
  }
}

class App extends React.Component {
  render() {
    return <h1>Hello, {<Text color="darkgray" value="Wesley" />} </h1>;
  }
}

export default App;
